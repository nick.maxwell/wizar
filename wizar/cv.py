import numpy as np
import dataclasses
from scipy.spatial.transform import Rotation as BaseRotation
from typing import *


class Rotation(BaseRotation):
    @classmethod
    def from_HPR_RDF(cls, heading: float, pitch: float, roll: float):
        """
        Construct from heading, pitch, roll, in degrees, in Right-Down-Front coordinates.

        :param heading:
        :param pitch:
        :param roll:
        :return:
        """
        return cls.from_euler('YXZ', (heading, pitch, roll), degrees=True)

    def RDF_to_RFU(self) -> "Rotation":
        """
        Right-Down-Front to Right-Front-Up

        :return:
        """

        Rx = Rotation.from_euler('X', 90, degrees=True)
        return Rx.inv() * self * Rx


@dataclasses.dataclass
class Intrinsics:
    width: int
    height: int
    camera_matrix: np.ndarray = None
    distortion_coefficients: np.ndarray = None

    @property
    def is_valid(self):
        return self.camera_matrix is not None and self.distortion_coefficients is not None

    @property
    def fx(self):
        return self.camera_matrix[0, 0]

    @property
    def fy(self):
        return self.camera_matrix[1, 1]

    @property
    def cx(self):
        return self.camera_matrix[0, 2]

    @property
    def cy(self):
        return self.camera_matrix[1, 2]

    @property
    def hfov(self):
        return 2 * np.arctan(self.width / (2 * self.fx)) * 180 / np.pi

    @property
    def vfov(self):
        return 2 * np.arctan(self.height / (2 * self.fy)) * 180 / np.pi

    @classmethod
    def from_fov(cls, width: int, height: int, hfov: float):
        fx = 1 / (2 * np.tan((hfov / 2) * np.pi / 180) / width)
        fy = fx
        cx = width / 2
        cy = height / 2
        mat = np.zeros((3, 3))
        mat[0, 0] = fx
        mat[1, 1] = fy
        mat[0, 2] = cx
        mat[1, 2] = cy
        mat[2, 2] = 1
        return cls(width, height, mat, np.zeros(5))


@dataclasses.dataclass
class Pose:
    orientation: Rotation
    position: Union[np.ndarray, Tuple[float, ...]]

    def __post_init__(self):
        self.position = np.array(self.position, dtype='float64')

    @classmethod
    def identity(cls):
        return cls(Rotation.identity(), np.ndarray((0, 0, 0)))

    @classmethod
    def from_HPR_RDF(cls, heading: float, pitch: float, roll: float, position: np.ndarray = None):
        """
        Construct from heading, pitch, roll, and position, in Right-Down-Front coordinates.

        :param heading:
        :param pitch:
        :param roll:
        :param position:
        :return:
        """
        if position is None:
            position = np.zeros(3)
        orientation = Rotation.from_euler('YXZ', (heading, pitch, roll), degrees=True)
        return cls(orientation, position)

    def apply(self, x: np.ndarray):
        return self.orientation.apply(x) + self.position

    def __matmul__(self, other: Union["Pose", Rotation]) -> "Pose":
        # R2 (R1 x + t1) + t2 = R2 R1 x + (R2 t1 + t2)
        if isinstance(other, Rotation):
            return self.__class__(self.orientation * other, self.position)
        return self.__class__(self.orientation * other.orientation, self.apply(other.position))

    def inv(self) -> "Pose":
        # Rx + t = y -> Rinv(y - t) = Rinv y - Rinv t = x
        Rinv = self.orientation.inv()
        return self.__class__(Rinv, -Rinv.apply(self.position))

    def as_matrix(self):
        mat = np.zeros((3, 4))
        mat[:, :3] = self.orientation.as_matrix()
        mat[:, 3] = self.position
        return mat

    @classmethod
    def from_rvec_tvec(cls, rvec: np.ndarray, tvec: np.ndarray):
        return cls(Rotation.from_rotvec(rvec), tvec)

    def __repr__(self):
        rvec = self.orientation.as_rotvec()
        ang = np.linalg.norm(rvec)
        if abs(ang) > 1E-12:
            uvec = rvec / ang
        else:
            uvec = np.array((1, 0, 0))
        ang *= 180 / np.pi
        return f"Pose(rot-axis={uvec.round(3)}, rot={ang:.3f}, pos={self.position.round(3)})"

    def RDF_to_RFU(self) -> "Pose":
        """
        Right-Down-Front to Right-Front-Up

        :return:
        """

        Rx = Pose(Rotation.from_euler('X', 90, degrees=True), (0, 0, 0))
        return Rx @ self @ Rx.inv()
