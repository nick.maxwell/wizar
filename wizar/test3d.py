#!/usr/bin/env python
import sys, os
import numpy as np
from direct.showbase.ShowBase import ShowBase
from panda3d.core import GeomVertexFormat, GeomVertexData, Geom, GeomVertexWriter, GeomTriangles, NodePath, \
    WindowProperties, LQuaternionf
from scipy.spatial.transform.rotation import Rotation
from wizar.cv import *
from typing import *
import aruco_cube
import tempfile
import pathlib
import string
from pathlib import Path
import panda3d.core
import dataclasses


@dataclasses.dataclass
class DisplayCube:
    aruco: aruco_cube.ArcuoCube
    node_path: NodePath
    size: float
    position: Union[np.ndarray, Tuple[float, ...]]
    HPR: Union[np.ndarray, Tuple[float, ...]]
    pose: Pose = None

    def __post_init__(self):
        self.position = np.array(self.position, dtype='float64')
        self.HPR = np.array(self.HPR, dtype='float64')
        if self.pose is None:
            self.update()

    @classmethod
    def from_attached(cls, app: "TestApp", size: float):
        adict = aruco_cube.ArucoDict(4, 100)
        cube = aruco_cube.ArcuoCube(.0254, aruco_cube.ArcuoCubeIDs(1, 2, 3, 4, 5, 6), adict)
        path1 = (app.data_dir / 'cube.obj').absolute()
        path1.parent.mkdir(exist_ok=True, parents=True)
        parts = list(path1.parts)
        if 'win32' == sys.platform:
            parts[0] = dict((f"{A}:\\", f"/{A.lower()}") for A in string.ascii_uppercase)[parts[0]]
        path2 = '/'.join(parts)
        cube.create_obj_mesh(path1)
        cube_node = app.loader.load_model(path2)
        cube_path = NodePath(cube_node)
        cube_path.reparent_to(app.root_node_path)
        return cls(cube, cube_path, size, (0, 0, 0), (0, 0, 0))

    def update(self):
        self.node_path.setScale(self.size / .0254)
        self.position = np.array(self.position, dtype='float64')
        self.HPR = np.array(self.HPR, dtype='float64')
        R = Rotation.from_HPR_RDF(*self.HPR).RDF_to_RFU()
        self.pose = Pose(R, self.position)
        self.node_path.setPos(*self.position)
        self.node_path.setQuat(_p3dquat(self.pose.orientation))


class TestApp(ShowBase):
    intrinsics: Intrinsics
    cube: DisplayCube
    camera_pose: Pose
    root_node_path: NodePath
    data_dir: Path

    def update_pose(self):
        R = Rotation.from_HPR_RDF(self.heading, self.pitch, self.roll).RDF_to_RFU()
        self.pose = Pose(R, np.array(self.position))
        self.update_camera()

    def __init__(self):
        super().__init__(self)
        self.data_dir = get_datadir() / 'test3d'
        self.data_dir.mkdir(exist_ok=True, parents=True)

        self.intrinsics = Intrinsics.from_fov(1024, 768, 60)
        self.camera_pose = Pose(Rotation.identity(), (0, 0, 0))
        props = WindowProperties()
        props.setSize(self.intrinsics.width, self.intrinsics.height)
        self.win.requestProperties(props)
        self.setBackgroundColor(.1, .1, .1)
        self.update_camera()
        self.disableMouse()

        self.displayRegion = self.camNode.getDisplayRegion(0)

        self.root_node_path = self.render.attachNewNode("Root")
        self.root_node_path.setPos(0.0, 0.0, 0.0)
        self.root_node_path.setScale(1, 1, 1)

        self.cube = DisplayCube.from_attached(self, .2)
        self.cube.position = (0, 1, 0)
        self.cube.update()

        self.accept("escape", sys.exit)
        self.accept("o", self.oobe)

        def cube_move(x, y, z):
            self.cube.position += np.array((x, y, z), dtype='float64')
            self.cube.update()

        def cube_rotate(h, p, r):
            self.cube.HPR += np.array((h, p, r), dtype='float64')
            self.cube.update()

        def test(*args):
            image = self.get_screen_shot()
            result = self.cube.aruco.detect(image, self.intrinsics.camera_matrix,
                                            self.intrinsics.distortion_coefficients)
            if result is not None:
                rvec, tvec = result
                pose = Pose.from_rvec_tvec(rvec, tvec).RDF_to_RFU()

                print('-----')
                print(self.cube.pose)
                print(pose)

        self.accept("d", cube_move, [0.1, 0.0, 0.0])
        self.accept("a", cube_move, [-0.1, 0.0, 0.0])
        self.accept("w", cube_move, [0.0, 0.0, 0.1])
        self.accept("s", cube_move, [0.0, 0.0, -0.1])
        self.accept("e", cube_move, [0.0, 0.1, 0.0])
        self.accept("q", cube_move, [0.0, -0.1, 0.0])
        self.accept("h", cube_rotate, [10, 0.0, 0.0])
        self.accept("p", cube_rotate, [0, 10, 0.0])
        self.accept("r", cube_rotate, [0, 0.0, 10])

        self.accept("t", test, [])

    def update_camera(self):
        self.camLens.setNearFar(.1, 10)
        self.camLens.setFov(self.intrinsics.hfov)
        self.camera.setPos(*self.camera_pose.position)
        self.camera.setQuat(_p3dquat(self.camera_pose.orientation))

    def get_screen_shot(self) -> np.ndarray:
        tex = self.displayRegion.getScreenshot()
        buffer = tex.getRamImage()
        image = np.reshape(np.frombuffer(buffer, np.uint8), (tex.getYSize(), tex.getXSize(), tex.getNumComponents()))
        return image[::-1, ...].copy()


def get_datadir() -> pathlib.Path:
    """
    https://stackoverflow.com/questions/19078969/python-getting-appdata-folder-in-a-cross-platform-way

    Returns a parent directory path
    where persistent application data can be stored.

    # linux: ~/.local/share
    # macOS: ~/Library/Application Support
    # windows: C:/Users/<USER>/AppData/Roaming
    """

    home = pathlib.Path.home()

    if sys.platform == "win32":
        return home / "AppData/Roaming"
    elif sys.platform == "linux":
        return home / ".local/share"
    elif sys.platform == "darwin":
        return home / "Library/Application Support"


def _p3dquat(rotation: Rotation) -> LQuaternionf:
    x, y, z, w = rotation.as_quat()
    return LQuaternionf(w, x, y, z)


if __name__ == "__main__":
    app = TestApp()
    app.run()
