import numpy as np
import cv2
import math
from pynput.keyboard import Key, Controller
from pynput import keyboard
# import pylab as plt
import skimage.morphology
import skimage.measure
from collections import deque
import multiprocessing
from multiprocessing import Queue, Process, Event
from scipy.spatial.transform import Rotation
import dataclasses
from typing import *
import queue as queue_module
import threading
import concurrent.futures
import time as time_module
from math import ceil, floor
import jsonpickle
from pathlib import Path
import logging
import pickle
import datetime
from dataclasses import dataclass
from direct.showbase.ShowBase import ShowBase
from panda3d.core import GeomVertexFormat, GeomVertexData, Geom, GeomVertexWriter, GeomTriangles, NodePath, \
    WindowProperties
import skvideo.io
from wizar.cv import *

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    datefmt='%m-%d %H:%M')

logger = logging.getLogger(__name__)


@dataclasses.dataclass
class ArucoCube:
    size: float
    marker_size: float
    frontID: int
    rightID: int
    leftID: int
    backID: int
    topID: int
    bottomID: int
    all_IDs: np.ndarray = None
    all_object_points: np.ndarray = None
    front_rot: Rotation = None
    right_rot: Rotation = None
    left_rot: Rotation = None
    back_rot: Rotation = None
    top_rot: Rotation = None
    bottom_rot: Rotation = None
    rot_map: dict = None
    translation_map: dict = None

    def wireframe(self):
        return np.array([
            ((1, 1, 1), (-1, 1, 1)),
            ((1, 1, -1), (-1, 1, -1)),
            ((1, -1, 1), (-1, -1, 1)),
            ((1, -1, -1), (-1, -1, -1)),
            ((1, 1, 1), (1, -1, 1)),
            ((1, 1, -1), (1, -1, -1)),
            ((-1, 1, 1), (-1, -1, 1)),
            ((-1, 1, -1), (-1, -1, -1)),
        ]) * self.size / 2

    def object_points(self, IDs: np.ndarray):
        np.isin(self.allIDs)

    def __post_init__(self):
        self.rot_map = {
            self.frontID: self.front_rot,
            self.rightID: self.right_rot,
            self.leftID: self.left_rot,
            self.backID: self.back_rot,
            self.topID: self.top_rot,
            self.bottomID: self.bottom_rot,
        }
        s = self.size / 2
        self.translation_map = {
            self.frontID: [0, -s, 0],
            self.backID: [0, s, 0],
            self.rightID: [-s, 0, 0],
            self.leftID: [s, 0, 0],
            self.topID: [0, 0, -s],
            self.bottomID: [0, 0, s]
        }
        self.all_IDs = np.array((self.frontID, self.topID, self.rightID, self.leftID, self.backID, self.bottomID),
                                dtype=np.int64)
        self.all_object_points = np.zeros((6, 4, 3))
        self.all_object_points[0, 0, :] = [-s, ]


# for i in range(10):
#     print(i)
#     cap = cv2.VideoCapture(i)
#     test, frame = cap.read()
#     print("i : " + str(i) + " /// result: " + str(test))

@dataclasses.dataclass
class Task:
    display: bool
    func: Callable
    args: Iterable[Any] = tuple()
    kwargs: Dict = dataclasses.field(default_factory=dict)

    def run(self):
        return self.func(*self.args, **self.kwargs)


@dataclasses.dataclass
class TaskResult:
    display_image: np.ndarray = None
    display_pause: float = None
    result: Any = None


def _process(
        stop_event: Event,
        input_queue: Queue,
        output_queue: Queue,
        display_queue: Queue):
    stop = False
    while not stop:
        stop = stop_event.is_set()
        while True:
            try:
                task = input_queue.get(timeout=1.0)
                result = task.run()
                if result is None:
                    continue
                result: TaskResult
                if result.display_image is not None:
                    display_queue.put(DisplayInfo('frame', result.display_image, result.display_pause))
                if result.result is not None:
                    output_queue.put(result.result)
            except queue_module.Empty:
                break


@dataclasses.dataclass
class DisplayInfo:
    name: str
    image: np.ndarray
    delay: float = 0


def _display_process(
        stop_event: Event,
        display_queue: Queue):
    names = []
    stop = False
    while not stop:
        stop = stop_event.is_set()
        while True:
            try:
                info = display_queue.get(timeout=1.0)
                info: DisplayInfo
                name = info.name
                image = info.image
                if name not in names:
                    cv2.namedWindow(name, cv2.WINDOW_NORMAL)
                else:
                    names.append(name)
                cv2.imshow(name, image)
                delay = 1
                if info.delay is not None and info.delay > 0:
                    delay = int(ceil(delay * 1000))
                cv2.waitKey(delay)
            except queue_module.Empty:
                break


def _collect_items(stop_event: threading.Event, queue: Queue):
    items = []
    stop = False
    while not stop:
        stop = stop_event.is_set()
        while True:
            try:
                item = queue.get(timeout=1.0)
                items.append(item)
                print('item', len(items))
            except queue_module.Empty:
                break
    return items


def find_chessboard_corners(image, pattern_size):
    _image = image[:, ::-1, :].copy()
    ret, corners = cv2.findChessboardCorners(_image, pattern_size, None)

    if ret:
        txt = 'Corners Found'

        for i in range(corners.shape[0]):
            pt = corners[i, 0, :].astype('int32')
            cv2.circle(_image, tuple(pt), 1, (0, 0, 255), -1)
            image = _image[:, ::-1, :]
    else:
        txt = 'Not Found'
        corners = None

    cv2.putText(image, txt, (50, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2, cv2.LINE_AA)

    return TaskResult(display_image=image, result=corners, display_pause=0.5)


@dataclasses.dataclass
class CharucoCalibration:
    @dataclasses.dataclass
    class Detection:
        corners: np.ndarray
        markerIDs: np.ndarray
        object_points: np.ndarray
        image_points: np.ndarray
        image: np.ndarray

    def __init__(self):
        self.squares = (5, 7)
        self.aruco_dict = cv2.aruco.getPredefinedDictionary(cv2.aruco.DICT_6X6_250)
        self.board = cv2.aruco.CharucoBoard_create(
            *self.squares, .04, .02, self.aruco_dict)

    def write_calibration_board(self, path):
        squares = self.squares
        board = self.board
        board_image = board.draw((100 * squares[0], 100 * squares[1]), 10, 1)
        plt.figure(figsize=(8.5, 11))
        plt.imshow(board_image.astype('float32'), cmap='gray', interpolation='nearest')
        plt.tight_layout()
        plt.axis('off')
        plt.savefig(path)

    def write_charuco_diamond(self, path):
        diamond = cv2.aruco(
            *self.squares, .04, .02, self.aruco_dict)

    def detect(self, image: np.ndarray) -> Optional["CharucoCalibration.Detection"]:
        image = image.copy()
        params = cv2.aruco.CORNER_REFINE_NONE
        marker_corners, markerIDs, rejected = cv2.aruco.detectMarkers(image, self.aruco_dict)
        if markerIDs is not None and len(markerIDs) > 0:
            result = cv2.aruco.interpolateCornersCharuco(
                marker_corners, markerIDs, image, self.board)
            num, charuco_corners, charucoIDs = result
            if num > 0:
                cv2.aruco.drawDetectedCornersCharuco(image, charuco_corners, charucoIDs, (255, 0, 0))
                object_points, image_points = cv2.aruco.getBoardObjectAndImagePoints(
                    self.board, charuco_corners, charucoIDs)
                return CharucoCalibration.Detection(charuco_corners, charucoIDs, object_points, image_points, image)
        return None

    def get_calibration(self, samples: List["CharucoCalibration.Detection"], width: int, height: int,
                        charuco_method=True) -> Intrinsics:
        #     with open('temp_samples.pickle', 'wb') as f:
        #         pickle.dump(samples, f)
        valid_samples = [sample for sample in samples if sample.corners.shape[0] >= 4]
        num_samples = len(samples)
        if num_samples < 3:
            raise ValueError('insufficient valid samples')
        if charuco_method:
            result = cv2.aruco.calibrateCameraCharucoExtended(
                [sample.corners for sample in valid_samples],
                [sample.markerIDs for sample in valid_samples],
                self.board,
                (width, height),
                None, None
            )
        else:
            object_points = [s.object_points[:, 0, :].copy() for s in valid_samples]
            image_points = [s.image_points[:, 0, :].copy() for s in valid_samples]
            result = cv2.calibrateCameraExtended(
                object_points, image_points, (width, height), None, None)

        RMS, cam_matrix, distortion_coefficients, rvecs, tvecs, int_stdev, ext_stdev, RMSs = result
        diam = np.sqrt(width * width + height * height)
        med_RMS = np.median(RMSs / diam)
        print(f'Calibration: count = {num_samples}, RMS = {med_RMS * 100: 3.1f}%')

        intrinsics = Intrinsics(
            width=width, height=height, camera_matrix=cam_matrix,
            distortion_coefficients=distortion_coefficients)

        return intrinsics


def find_charuco_corners(image):
    mir_image = image[:, ::-1, :].copy()
    calib = CharucoCalibration()
    detection = calib.detect(mir_image)
    if detection is not None:
        txt = 'Corners Found'
        corners = detection
        image = detection.image[:, ::-1, :]
    else:
        txt = 'Not Found'
        corners = None
    image = image.astype('uint8')
    cv2.putText(image, txt, (50, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2, cv2.LINE_AA)
    return TaskResult(display_image=image, result=detection, display_pause=0.5)


@dataclass
class WizAR:
    cam_index: int = 0
    num_workers: int = 8
    intrinsics: Optional[Intrinsics] = None
    record_stream_destination: str = None
    playback_stream_source: str = None

    def read_camera_calibration(self):
        if self.camera_calibration_record_path.exists():
            with open(self.camera_calibration_record_path, 'rb') as f:
                return pickle.loads(f.read())

    def save_camera_calibration(self):
        if self.intrinsics.is_valid:
            record = self.read_camera_calibration()
            if record is None:
                record = {}
            record[self.cam_index] = self.intrinsics
            with open(self.camera_calibration_record_path, 'wb') as f:
                f.write(pickle.dumps(record))

    def write_calibration_board(self, path):
        calib = CharucoCalibration()
        calib.write_calibration_board(path)

    def __post_init__(self):
        self.camera_calibration_record_path = Path("wizar_camera_cal.pickle")

        cam_cal = self.read_camera_calibration()
        if cam_cal is not None and self.cam_index in cam_cal:
            self.intrinsics = cam_cal[self.cam_index]
            if self.intrinsics.is_valid:
                print('valid camera calibration loaded')
        else:
            self.intrinsics = Intrinsics(height=720, width=1280)

        self.aruco_dict = cv2.aruco.getPredefinedDictionary(cv2.aruco.DICT_4X4_1000)

        self.input_queue = Queue()
        self.output_queue = Queue()
        self.display_queue = Queue()
        self.stop_event = Event()

        self.processes = []
        for i in range(self.num_workers):
            p = Process(target=_process,
                        args=(self.stop_event, self.input_queue, self.output_queue, self.display_queue))
            p.start()
            self.processes.append(p)

        p = Process(target=_display_process, args=(self.stop_event, self.display_queue))
        p.start()
        self.processes.append(p)

        self.height = self.intrinsics.height
        self.width = self.intrinsics.width
        if self.playback_stream_source is None:
            self.cap = cv2.VideoCapture(self.cam_index)
            self.cap.set(3, self.width)
            self.cap.set(4, self.height)
        else:
            self.cap = cv2.VideoCapture(str(self.playback_stream_source))

        key_thread = keyboard.Listener(on_press=lambda key: self.on_press(key))
        key_thread.start()
        self.position_deque = deque(maxlen=30)
        self.target_color = None
        self.target_descriptor = None

        self.orb = cv2.ORB_create()
        self.max_matches = 50
        FLANN_INDEX_KDTREE = 1
        index_params = dict(algorithm=FLANN_INDEX_KDTREE, trees=5)
        search_params = dict(checks=50)  # or pass empty dictionary
        self.flann = cv2.FlannBasedMatcher(index_params, search_params)

        orange = np.array([50, 38, 131], dtype='uint8')
        blue = np.array([142, 68, 0], dtype='uint8')
        self.target_color = blue
        self.position = None

        self.TRACK = False
        self.CALIBRATE = False
        self.CAMERA_CALIBRATION = False
        self.EXIT = False
        if self.target_color is None:
            self.CALIBRATE = True

        self.cubes = [
            ArucoCube(
                size=.03,
                marker_size=.022,
                frontID=91,
                topID=90,
                rightID=93,
                leftID=89,
                backID=88,
                bottomID=92,
                top_rot=Rotation.from_euler('Z', -90, degrees=True),
                front_rot=Rotation.from_euler('YZ', (-90, -90), degrees=True),
                right_rot=Rotation.from_euler('Y', -90, degrees=True),
                left_rot=Rotation.from_euler('YZ', (-90, -180), degrees=True),
                back_rot=Rotation.from_euler('ZX', (+90, -90), degrees=True),
                bottom_rot=Rotation.from_euler('ZY', (+90, 180), degrees=True)
            )
        ]

        self.cube_dict = {}
        for cube in self.cubes:
            self.cube_dict[cube.frontID] = cube
            self.cube_dict[cube.topID] = cube
            self.cube_dict[cube.rightID] = cube
            self.cube_dict[cube.leftID] = cube
            self.cube_dict[cube.backID] = cube
            self.cube_dict[cube.bottomID] = cube

        if self.record_stream_destination is not None:
            self.record__writer = skvideo.io.FFmpegWriter(self.record_stream_destination)

    def close(self):
        try:
            if self.cap is not None:
                self.cap.release()
        except:
            pass
        try:
            self.record__writer.close()
        except:
            pass
        try:
            cv2.destroyAllWindows()
        except:
            pass

        try:
            self.stop_event.set()
        except:
            pass
        try:
            for p in self.processes:
                p.join()
        except:
            pass

    def flush_io_queues(self, timeout=.5):
        while True:
            try:
                self.input_queue.get(timeout=timeout)
            except queue_module.Empty:
                break

        while True:
            try:
                self.output_queue.get(timeout=timeout)
            except queue_module.Empty:
                break

    def display(self, image: np.ndarray, delay: float = 0, mirror=False):
        if mirror:
            image = image[:, ::-1, :].copy()
        self.display_queue.put(DisplayInfo('frame', image, delay=delay))

    def record(self, image: np.ndarray):
        if self.record_stream_destination is not None:
            self.record__writer.writeFrame(image)

    def get_frame(self, mirrored=True):
        test, image = self.cap.read()
        self.record(image)
        if test:
            image = image.astype('uint8')
            if mirrored:
                image = image[:, ::-1, :]
            return image

    def on_press(self, key):
        try:
            _char = key.char
            if _char == 'c':
                self.CALIBRATE = True
            if _char == 'k':
                self.CAMERA_CALIBRATION = True
            if _char == 'q':
                self.EXIT = True
                return
            if _char == 's':
                return
            if _char == 't':
                if self.TRACK:
                    self.TRACK = False
                else:
                    self.TRACK = True
            if _char == 'x':
                self.position_deque.clear()

        except AttributeError:
            print('special key {0} pressed'.format(
                key))

    def color_transform(self, image):
        # image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        return image

    def get_features(self, image):
        kp, des = self.orb.detectAndCompute(image, None)
        return kp, des

    def _cam_cal_keypress(self, key):
        try:
            _char = key.char
            if _char == 'q':
                self.EXIT = True
                return
            if _char == 'n':
                self.CAMERA_CALIBRATION_NEXT = True
            if _char == 'd':
                self.CAMERA_CALIBRATION_DONE = True
                return

        except AttributeError:
            print('special key {0} pressed'.format(
                key))

    def camera_calibration(self):
        print('Camera calibration...')
        pattern_size = (7, 9)
        self.CAMERA_CALIBRATION = False
        self.CAMERA_CALIBRATION_NEXT = False
        self.CAMERA_CALIBRATION_DONE = False
        t0 = time_module.time()
        last_display_time = None

        with concurrent.futures.ThreadPoolExecutor(max_workers=1) as executor:
            key_thread = keyboard.Listener(on_press=lambda key: self._cam_cal_keypress(key))
            key_thread.start()

            self.flush_io_queues()
            collect_stop = threading.Event()
            future = executor.submit(_collect_items, collect_stop, self.output_queue)

            while True:

                if self.EXIT:
                    # key_thread.join()
                    return

                image = self.get_frame()
                if image is None:
                    continue

                if self.CAMERA_CALIBRATION_NEXT:
                    self.CAMERA_CALIBRATION_NEXT = False
                    # self.input_queue.put(Task(True, find_chessboard_corners, (image, pattern_size)))
                    self.input_queue.put(Task(True, find_charuco_corners, (image,)))
                else:
                    if last_display_time is None or time_module.time() - last_display_time > .1:
                        self.display(image)
                        last_display_time = time_module.time()

                if self.CAMERA_CALIBRATION_DONE:
                    break

                if self.CAMERA_CALIBRATION:
                    break

            collect_stop.set()
            samples = future.result()
            num_samples = len(samples)
            if num_samples > 3:
                calib = CharucoCalibration()
                timestamp = datetime.datetime.now().isoformat().replace(':', '-')
                with open(f'temp_samples_{timestamp}.pickle', 'wb') as f:
                    pickle.dump(samples, f)
                self.intrinsics = calib.get_calibration(samples, self.width, self.height)
                self.save_camera_calibration()

                # n, m = pattern_size
                # objp = np.zeros((n * m, 3), np.float32)
                # objp[:, :2] = np.mgrid[0:m, 0:n].T.reshape(-1, 2)
                # object_points = [objp, ] * num_samples
                # ret, cam_matrix, distortion_coefficients, rvecs, tvecs = cv2.calibrateCamera(
                #     object_points, image_points, (self.width, self.height), None, None)
                # print(f'Calibration: count = {num_samples}, RMS = {ret:.1f}')
                # self.intrinsics = Intrinsics(
                #     width=self.width, height=self.height, camera_matrix=cam_matrix,
                #     distortion_coefficients=distortion_coefficients)
                # self.save_camera_calibration()
            else:
                print('Calibration: too few points to calibrate')
        key_thread.stop()

    def run(self):

        while True:
            if self.EXIT:
                break

            if self.CALIBRATE:
                self.calibrate()
                continue

            if self.CAMERA_CALIBRATION:
                self.CAMERA_CALIBRATION = False
                self.camera_calibration()
                continue

            image = self.get_frame()
            if image is None:
                continue
            image = image.astype('uint8')

            # if pipeline.target_color is not None:
            #     image[:-1, :30] = pipeline.target_color

            # pipeline.find_wand_colormatch(image)
            # points = self.find_wand_featurematch(image)
            points = self.find_wand_aruco(image)
            # if points is not None:
            #     for i in range(len(points)):
            #         x = int(round(points[i, 0]))
            #         y = int(round(points[i, 1]))
            #         cv2.circle(image, (x, y), 3, (0, 255, 0), -1)

            if self.position is not None:
                cv2.circle(image, self.position, 7, (0, 0, 0), -1)
                cv2.circle(image, self.position, 2, (255, 255, 255), -1)

            for i, xy in enumerate(self.position_deque):
                cv2.circle(image, xy, 3, (255, 0, 255), -1)

            last = None
            for i, xy in enumerate(self.position_deque):
                if last is not None:
                    cv2.line(image, xy, last, (255, 0, 255))

                last = xy

            self.display(image)

        # When everything done, release the capture
        print('closing')
        self.close()
        print('done!')

    def update_position(self, x, y):
        x = int(round(x))
        y = int(round(y))
        self.position = (x, y)
        if self.TRACK:
            self.position_deque.append(self.position)

    def find_wand_aruco(self, image):
        points = None
        flipped = image[:, ::-1, :].copy()
        # flipped = image
        if self.intrinsics.is_valid:
            result = cv2.aruco.detectMarkers(flipped, dictionary=self.aruco_dict)
            corners, IDs, rejected = result
            if len(corners) > 0:

                # for i in range(len(corners)):
                #     cv2.aruco.drawAxis(
                #         flipped,
                #         self.intrinsics.camera_matrix,
                #         self.intrinsics.distortion_coefficients,
                #         rvecs[i], tvecs[i], .022
                #     )
                #     pt = (int(corners[i][0][0][0]), int(corners[i][0][0][1]))
                #     txt = f"{IDs[i, 0]}"
                #     font = cv2.FONT_HERSHEY_SIMPLEX
                #     font_scale = .5
                #     cv2.putText(flipped, txt, pt, font, font_scale, color=(0, 0, 0), bottomLeftOrigin=False,
                #                 thickness=3)
                #     cv2.putText(flipped, txt, pt, font, font_scale, color=(255, 255, 255),
                #                 bottomLeftOrigin=False,
                #                 thickness=1)

                cv2.aruco.drawDetectedMarkers(flipped, corners)

                # for i in range(len(corners)):
                #     cv2.aruco.drawAxis(
                #         flipped,
                #         self.intrinsics.camera_matrix,
                #         self.intrinsics.distortion_coefficients,
                #         rvecs[i], tvecs[i], .022
                #     )

                points = []
                for i, ID in enumerate(IDs.flatten()):
                    cube = self.cube_dict.get(ID)

                    if cube is not None:
                        rvecs, tvecs, obj_points = cv2.aruco.estimatePoseSingleMarkers(
                            corners, cube.marker_size, self.intrinsics.camera_matrix,
                            self.intrinsics.distortion_coefficients)
                        rot = Rotation.from_rotvec(rvecs[i].flatten()) * cube.rot_map[ID]
                        rvecs[i] = rot.as_rotvec()
                        tvecs[i] += rot.apply(cube.translation_map[ID])

                        # cv2.drawFrameAxes(
                        #     flipped, self.intrinsics.camera_matrix, self.intrinsics.distortion_coefficients,
                        #     rvecs[i], tvecs[i], cube.marker_size, thickness=3)

                        impoint, _ = cv2.projectPoints(
                            tvecs[i], np.zeros(3), np.zeros(3), self.intrinsics.camera_matrix,
                            self.intrinsics.distortion_coefficients)
                        points.append(impoint.flatten())

                        if i == 0:
                            for pt1_3D, pt2_3D in cube.wireframe():
                                pt1_3D = rot.apply(pt1_3D) + tvecs[i]
                                pt2_3D = rot.apply(pt2_3D) + tvecs[i]
                                impoint, _ = cv2.projectPoints(
                                    np.array([pt1_3D, pt2_3D]), np.zeros(3), np.zeros(3), self.intrinsics.camera_matrix,
                                    self.intrinsics.distortion_coefficients)
                                cv2.line(flipped, tuple(impoint[0].round().astype('int').flatten()),
                                         tuple(impoint[1].round().astype('int').flatten()),
                                         color=(0, 0, 255), thickness=5, )

                if len(points) > 0:
                    x, y = np.mean(points, axis=0)
                    x = self.width - x
                    self.update_position(x, y)

                image[...] = flipped[:, ::-1, :]

        return None


if __name__ == "__main__":
    # cal = CharucoCalibration()
    # cal.write_calibration_board('charuco_board.pdf')
    # #'temp_samples_2020-04-04T23-24-26.668291.pickle
    # with open('temp_samples.pickle', 'rb') as f:
    #     samples = pickle.load(f)
    # intrinsics = cal.get_calibration(samples, 1280, 720)

    wizar = WizAR(cam_index=1, )  # playback_stream_source=r"D:\WizAR\videos\test1.mp4")

    # record_stream_destination=r"D:\WizAR\videos\test.mp4")
    wizar.run()
