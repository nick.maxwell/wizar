from distutils.core import setup

setup(name='WizAR',
      version='0.0',
      description='',
      author='Nick Maxwell',
      author_email='nicholas.maxwell@gmail.com',
      url='https://gitlab.com/nick.maxwell/wizar',
      packages=['wizar'],
      )
